# Note de clarification - N°5 Complexe cinématographique

Contexte
---------------------------
Je suis en charge de créer une base de données pour un nouveau complexe cinématographique afin de pouvoir gérer le fonctionnement quotidien et en particulier la gestion des films et des entrées.

Objectifs
---------------------------
La base de données doit être capable de répondre aux besoins suivants: 
- Présenter les caractéristiques d'un film 
- Faciliter la gestion d'un ensemble de films pour les distributeurs
- Permettre aux spectateurs de noter un film (entre 0 et 5)
- Assurer la gestion des séances de cinéma proposées
- Assurer la gestion des salles attribuées pour un film
- Permettre au complexe de proposer des abonnements à la projection d'un film
- Proposer le tarif adéquat pour un ticket unitaire
- Vérifier qu'il reste encore des places sur la carte depuis le dernier rechargement pour un ticket tiré d'une carte d'abonnement
- Permettre la vente de produits (boisson ou alimentaire) pour les vendeurs
- Etablir différentes statistiques, comme par exemple le pourcentage d'occupation des séances, la mesure du succès d'un film, la part d'abonnés, le revenu généré par un film, le nombre de spectateurs par film, etc.

Acteurs
---------------------------
**Maîtrise d'ouvrage** : Chargé de TD de l'UV NF17 à l'UTC, Alessandro Victorino <br>
**Maîtrise d'oeuvre**: Sébastien DAM

Livrables
---------------------------
- **README.md**: un fichier comportant explicitant le contexte et la structure du projet
- **NDC:** Note de clarification
- **MCD**: un modèle UML au format plantuml
- **MLD**: un modèle logique au format plain text
- **CREATE et INSERT**: Code SQL pour la construction des tables et l'insertion d'un jeu de données.
- **SQL SELECT**: Code SQL pour la création des vues
- **PostgreSQL/JSON**: Manipulation de données JSON intégrées dans une base PostgreSQL

Contraintes
---------------------------
- Un livrable sur Git doit être rendu chaque semaine, reprenant les éléments de la partie Livrables.
- Le MCD doit être livré sous *plantUML*
- La base de données est réalisée sous PostgreSQL

Liste des objets
---------------------------
Dans le package **Personnes**, on a une classe mère qui identifie un individu et ses classes filles répertoriant les différents individus: 
 
- Individu (classe *abstraite*)
    - nom : text
    - prenom : text 
    - dateNaissance : date 


- Distributeur qui hérite de Individu
    - idDistributeur : integer {key}


- Realisateur qui hérite de Individu
    - idRealisateur : integer {key}


- Producteur qui hérite de Individu
    - idProducteur : integer {key}


- Vendeur qui hérite de Individu 
    - idProducteur : integer {key}


- Spectateur qui hérite de Individu
    - idSpectateur : integer {key}
    - nbPlaces : [-1, 50] correspond au nombre de places que possède le spectateur sur sa carte d'abonnement. S'il n'est pas abonné, sa valeur vaut -1. <br>

Dans le package **Projection**, on a les 3 classes concernant la gestion des films:

- Film
    - titre : text 
    - dateSortie : date 
    - genre : text
    - ageMin : integer > 0
    - duree : integer > 0, en minutes
    - distributeur : integer {clé étrangère vers Distributeur} <br>

Les attributs titre et dateSortie forment la clé primaire de Film. <br>

- Seance
    * idSeance : integer {key}
    * titre : text {clé étrangère vers Film}
    * dateSortie : date {clé étrangère vers Film}
    * jour : date
    * heure : time
    * doublage: {VF,VO,VOST,VI,VM,VQ} <br>
    * code : char(2) {clé étrangère vers Salle}

- Salle
    * code : char(2) {key}
    * nbPlaces : integer compris entre 1 et 100 <br>

**Remarque**: Le choix de la modélisation de la **carte d'abonnement** directement implémentée dans la classe **Spectateur** a été réalisé 
dans un soucis de clarté et d'efficacité. <br>
En effet, j'aurai pu ajouter une classe **CarteAbonnement** avec comme attribut le nombre de places disponibles sur cette carte. 
Celle-ci aurait été associée à Spectateur avec une cardinalité Spectateur 1--[0..1] CarteAbonnement (car un spectateur
ne peut posséder qu'une carte ou ne pas être abonné, et une carte n'appartient qu'à un spectateur). Puis, j'aurais eu une autre
association de carteAbonnement vers **TicketAbonnement**. <br>
Cependant, d'une part, comme il s'agit d'une association 1:1 j'ai décidé de fusionner les classes Spectateur et CarteAbonnement en 
une seule relation pour **réduire la complexité et le nombre de contraintes** induites par la présence de deux relations. 
D'autre part, dans le cadre du sujet, il n'est pas très utile de savoir quelle carte possède un spectateur et de quelle carte est 
tirée un ticket d'abonnement, cela imposerait au contraire d'autres contraintes supplémentaires. <br>
D'où la fusion de la notion de carte d'abonnement directement implémentée dans la classe Spectateur.

- Note
    * idSeance : integer {clé étrangère vers Seance}
    * idSpectateur : integer {clé étrangère vers Spectateur}
    * note : [0,5] <br>

Les attributs idSeance et idSpectateur forment la clé primaire de Note. <br>

- Produit
    - idSpectateur : integer {clé étrangère vers Spectateur}
    - idVendeur : integer {clé étrangère vers Vendeur}
    - produit : json (attribut multivalué)
    - prix : integer > 0 <br>


Dans le package **Entree**, on a la structure d'héritage pour les entrées: 

- Entree (classe *abstraite*)
    - idEntree : integer {clé locale}
    - idSeance : integer {clé étrangère vers Seance}
    - idSpectateur : integer {clé étrangère vers Spectateur}
    - idVendeur : integer {clé étrangère vers Vendeur} <br>


- TicketUnitaire **hérite** de Entree
    - tarif : json (attribut composé de tarif et du prix) <br>


- TicketAbonnement **hérite** de Entree
    - prix : integer > 0


Choix des associations
---------------------------
1. Associations **1:N**

- Distributeur 1 -- N Film : Un distributeur gère un ensemble de films.
- Film 1 -- N Seance : Un film est projeté sur plusieurs séances. Les séances ne correspondent qu'à un film.
- Seance N -- 1 Salle : Chaque séance n'a lieu que dans une seule salle. Une salle peut projeter plusieurs séances par jour.
- Spectateur 1 -- N Entree : Un spectateur peut acheter plusieurs entrées s'il/elle aime aller au cinéma. Une entrée n'est achetée que par une personne.
- Seance 1 -- 1..N Entree : Une séance possède au moins une entrée. Chaque entrée ne correspond spécifiquement qu'à une séance.

2. Associations **N:M** et classes d'association

- Spectateur N--M Seance : Un spectateur peut assister à plusieurs séances de cinéma. Une séance accueille différents spectateurs à la fois. <br>
    - Classe d'association Note : Un spectateur peut noter un film à l'issue de la séance. 
- Spectateur N--M Vendeur : Un spectateur peut acheter plusieurs produits à la fois au vendeur. Ce dernier vend alors ses biens à plusieurs spectateurs. <br>
    - Classe d'association Produit : Le produit (boisson/denrées alimentaires) est le bien vendu et consommé par le spectateur. 
- Realisateur 1..N--1..M Film : Plusieurs réalisateurs peuvent réaliser différents films.
- Producteur 1..N--1..M Film : Plusieurs producteurs peuvent produire différents films.

Les cardinalités de 1 minimum dans les associations 1:N et N:M vont exiger des contraintes de non-nullité et d'existence simultanée de tuples.

Choix des transformations d'héritage
---------------------------
Nous sommes en présence d'une transformation d'héritage pour la classe Individu.
1. La classe mère est abstraite.
2. Les classes filles possède une association avec d'autres classes, l'héritage est alors non complet.

On choisit donc l'**héritage par les classes filles**. 

Nous sommes en présence d'une transformation d'héritage pour la classe Entrée. 
1. L'héritage est presque complet car il n'y a qu'un attribut dans TicketUnitaire et pas d'association associée aux classes filles. 
2. La classe mère est abstraite et on ne gère que des tickets unitaires ou tickets tirés d'une carte d'abonnement; il n'y a aucune clé candidate identifiée. 

On choisit donc l'**héritage par les classes filles**. 

Nous aurions pu également modéliser une relation d'héritage entre les utilisateurs Distributeur, Spectateur et Vendeur mais nous avons décidé que 
cela n'apportait pas de réelles informations car la classe mère ne possèderait que les attributs nom et prenom et nous aurions alors 
eu un autre héritage par classes filles.

Liste des utilisateurs et leurs droits
---------------------------
- Distributeur : accès aux tables Film, Seance et Salle. C'est la personne qui gère la projection des films.
- Spectateur : accès aux tables Film, Seance (en SELECT), NOTE (en UPDATE). Il est le client qui consomme les biens et services. 
- Vendeur : accès aux tables Entree, Produit et la consultation de Séance. Il peut fixer les prix des produits et entrées et consulte les séances disponibles. 

Vues implémentées
---------------------------
L'implémentation des vues permet de s'assurer du bon fonctionnement de la base de données et l'expression de certains besoins: 
- vNbPlacesOccupees : recensement du nombre de places occupées pour chaque séance.
- vSpectateursPlaces : vérifie que le nombre de spectateurs est inférieur ou égal au nombre de places disponibles dans la salle. 
- vNbPlacesCarte : vérifie si le nombre de places disponibles dans la carte d'abonnement n'est pas négatif. 
- vOccupationSeance : calcule du pourcentage d'occupation des séances.
- vSuccesFilm : mesure le succès d'un film.
- vPartAbonnes : calcule la part d'abonnés.
- vRevenuFilm : calcule le revenu généré pour un film.
    - Les vues intermédiaires vRevenuFilmUnitaire et vRevenuFilmAbonne sont créées pour obtenir vRevenuFilm
- vNbSpectateurs : calcule le nombre de spectateurs par film
- vPlanning : affiche le planning des séances par salle
- vProduitPopulaires : classe les produits par popularité.