# MLD - N°5 Complexe cinématographique

Modèle logique relationnel
--------------------------

Tous les attributs sont non nuls hormis Note(note)

Distributeur(#idDistributeur: integer, nom: text, prenom: text, dateNaissance: date) avec (nom, prenom, dateNaissance) UNIQUE  <br>
Realisateur(#idRealisateur: integer, nom: text, prenom: text, dateNaissance: date) (nom, prenom, dateNaissance) UNIQUE  <br>
Producteur(#idProducteur: integer, nom: text, prenom: text, dateNaissance: date) (nom, prenom, dateNaissance) UNIQUE  <br>
Vendeur(#idVendeur: integer, nom: text, prenom: text, dateNaissance: date) (nom, prenom, dateNaissance) UNIQUE  <br>
Spectateur(#idSpectateur: integer, nom: text, prenom: text, dateNaissance: date, nbPlaces: [-1..50]) (nom, prenom, dateNaissance) UNIQUE 

Produit(#idSpectateur=>Spectateur, #idVendeur=>Vendeur, produit: **JSON**, prix: integer) avec prix > 0 <br>
Le produit pourra être n'importe quel boisson (coca, fanta, eau ...) et/ou aliment (popcorn, glace, snack ...). <br>
C'est donc un attribut multivalué. 

Film(#titre: text, #dateSortie: date,  genre: text, ageMin: integer, duree: integer, idDistributeur=>Distributeur) avec ageMin > 0, duree > 0 et en minutes <br>


FilmRealisateur(#titre=>Film(titre), #dateSortie=>Film(DateSortie), #idRealisateur=>Realisateur) <br>
FilmProducteur(#titre=>Film(titre), #dateSortie=>Film(DateSortie), #idProducteur=>Producteur) 

Seance(#idSeance: integer, titre=>Film(titre), dateSortie=>Film(dateSortie), jour: date, heure: time, doublage: {VF,VO,VOST,VI,VM,VQ}, code=>Salle) avec (jour,heure,code) UNIQUE <br>
Salle(#code: varchar(2), nbPlaces: integer) avec nbPlaces >= 1 et nbPlaces <= 100

Note(#idSeance=>Seance, #idSpectateur=>Spectateur, note: [0..5]) 

TicketUnitaire(#idEntree: integer, idSeance=>Seance, idSpectateur=>Spectateur, idVendeur=>Vendeur, tarif: **JSON**) avec (idSeance, idSpectateur) UNIQUE <br>
TicketAbonnement(#idEntree: integer, idSeance=>Seance, prix: integer, idSpectateur=>Spectateur, idVendeur=>Vendeur) avec (idSeance, idSpectateur) UNIQUE et prix > 0 <br>


Normalisation
--------------------------

Distributeur : 3NF <br>
Realisateur : 3NF<br>
Producteur : 3NF <br>
Vendeur : 3NF <br>
Spectateur : 3NF 

Produit : 3NF 

Film : 3NF 

FilmRealisateur : 3NF <br>
FilmProducteur : 3NF

Seance : 3NF 

Note : 3NF

TicketUnitaire : 1 NF <br>
idSpectateur -> tarif <br>
tarif -> prix <br>
Or, idSpectateur est une partie de la clé candidate (idSpectateur, idSeance)

Donc on normalise: <br>
TicketUnitaire(#idEntree: integer, idSeance=>Seance,  idSpectateur=>TarifSpectateur, idVendeur=>Vendeur) <br>
TarifSpectateur(#idSpectateur=>Spectateur, tarif: **JSON**) <br>
Tarif est un attribut composé du tarif du ticket ainsi que son prix. 

TicketAbonnement : 1 NF car idSpectateur -> prix 

donc on normalise: <br>
TicketAbonnement(#idEntree: integer, idSeance=>Seance, idSpectateur=>AbonnePrix, idVendeur=>Vendeur) avec (idSeance, idSpectateur) UNIQUE <br>
AbonnePrix(#idSpectateur=>Spectateur, prix: integer) avec prix > 0

Expression des vues
--------------------------

Contraintes de cardinalité minimale 1 dans les associations N:M:
- PROJECTION(Realisateur, idRealisateur) = PROJECTION(FilmRealisateur, idRealisateur) <br>
  AND <br>
  PROJECTION(Film, titre, dateSortie) = PROJECTION(FilmRealisateur, titre, dateSortie)
- PROJECTION(Producteur, idProducteur) = PROJECTION(FilmProducteur, idProducteur) <br>
  AND <br>
  PROJECTION(Film, titre, dateSortie) = PROJECTION(FilmProducteur, titre, dateSortie)

Contraintes de cardinalité minimale 1 dans les associations 1:N:
- TicketUnitaire.idSeance NOT NULL et PROJECTION(Seance, idSeance) = PROJECTION(TicketUnitaire, idSeance)
- TicketAbonnement.idSeance NOT NULL et PROJECTION(Seance, idSeance) = PROJECTION(TicketAbonnement, idSeance) 

Contraintes sur les tables TarifSpectateur et AbonnePrix: 
- TarifSpectateur.idSpectateur.nbPlaces = -1 <br>
  R1 = Projection(TarifSpectateur, idSpectateur) <br>
  R = Restriction(R1, nbPlaces = -1)


- AbonnePrix.idSpectateur.nbPlaces >= 0 <br>
  R1 = Projection(AbonnePrix, idSpectateur) <br>
  R = Restriction(R1, nbPlaces >= 0)

Le reste des vues concernant les statistiques, vérification ... sont exprimées en SQL dans le document SQL SELECT.

Gestion des droits des utilisateurs
-------------------------------------
CREATE GROUP Distributeur; <br>
GRANT ALL PRIVILEGES ON Fim, Seance, Salle TO Adherent; <br>

CREATE GROUP Spectateur; <br>
GRANT SELECT ON Film, Seance TO Spectateur; <br>
GRANT UPDATE ON Note TO Spectateur;

CREATE GROUP Vendeur; <br>
GRANT ALL PRIVILEGES ON Entree, Produit TO Vendeur; <br>
GRANT SELECT ON Seance TO Vendeur;