# NF17-D2-Projet2
Réalisation d'un projet individuel sur le sujet: <br>
**5. Complexe cinématographique** 

Structure
-------------------
- **README.md**: un fichier comportant explicitant le contexte et la structure du projet
- **NDC**: Note de clarification
- **MCD**: un modèle UML au format *plantuml*
- **MLD**: un modèle logique au format *plain text*
- **CREATE et INSERT**: Code SQL pour la construction des tables et l'insertion d'un jeu de données.
- **SQL SELECT**: Code SQL pour la création des vues 
- **PostgreSQL/JSON**: Manipulation de données JSON intégrées dans une base PostgreSQL